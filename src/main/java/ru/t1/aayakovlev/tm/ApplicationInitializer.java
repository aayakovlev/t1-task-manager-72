package ru.t1.aayakovlev.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.aayakovlev.tm.config.*;

public final class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @NotNull
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] { ApplicationConfig.class, DatabaseConfig.class, SecurityConfig.class };
    }

    @NotNull
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] { WebMVCApplicationConfig.class, WebWSApplicationConfig.class};
    }

    @NotNull
    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }

}
