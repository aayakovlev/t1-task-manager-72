package ru.t1.aayakovlev.tm.api.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/users")
public interface IUserRestEndpoint {

    @GetMapping(value = "/count", produces = APPLICATION_JSON_VALUE)
    long count() throws AbstractException;

    @DeleteMapping(value = "/delete", produces = APPLICATION_JSON_VALUE)
    void deleteAll() throws AbstractException;

    @DeleteMapping(value = "/delete/{id}", produces = APPLICATION_JSON_VALUE)
    void deleteById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException;

    @GetMapping(value = "/exists/{id}", produces = APPLICATION_JSON_VALUE)
    boolean existsById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException;

    @NotNull
    @GetMapping(value = "/find", produces = APPLICATION_JSON_VALUE)
    List<UserDTO> findAll() throws AbstractException;

    @NotNull
    @GetMapping(value = "/find/{id}", produces = APPLICATION_JSON_VALUE)
    UserDTO findById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException;

    @NotNull
    @PutMapping(value = "/save", produces = APPLICATION_JSON_VALUE)
    UserDTO save(
            @RequestBody @NotNull final UserDTO user
    ) throws EntityEmptyException;

    @NotNull
    @PostMapping(value = "/save/{id}", produces = APPLICATION_JSON_VALUE)
    UserDTO update(
            @RequestBody @NotNull final UserDTO user
    ) throws EntityEmptyException;

}
