package ru.t1.aayakovlev.tm.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.aayakovlev.tm.dto.soap.*;
import ru.t1.aayakovlev.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

public interface IProjectSoapEndpoint {

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectCountResponse count(
            @RequestPayload @NotNull final ProjectCountRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectDeleteAllResponse deleteAll(
            @RequestPayload @NotNull final ProjectDeleteAllRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectDeleteByIdResponse deleteById(
            @RequestPayload @NotNull final ProjectDeleteByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectExistsByIdResponse existsById(
            @RequestPayload @NotNull final ProjectExistsByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectFindAllResponse findAll(
            @RequestPayload @NotNull final ProjectFindAllRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectFindByIdResponse findById(
            @RequestPayload @NotNull final ProjectFindByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectSaveResponse save(
            @RequestPayload @NotNull final ProjectSaveRequest request
    ) throws EntityEmptyException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectUpdateRequest", namespace = ProjectSoapEndpoint.NAMESPACE)
    ProjectUpdateResponse update(
            @RequestPayload @NotNull final ProjectUpdateRequest request
    ) throws EntityEmptyException;

}
