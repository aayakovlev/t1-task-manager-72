package ru.t1.aayakovlev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.aayakovlev.tm.api.service.model.IUserService;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@Controller
@RequestMapping("/users")
public class UserController {

    @Autowired
    private IUserService service;

    @GetMapping("")
    @Secured({"ROLE_ADMINISTRATOR"})
    public ModelAndView index() throws AbstractException {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("user-list");
        modelAndView.addObject("users", service.findAll());
        return modelAndView;
    }

    @GetMapping("/delete/{id}")
    @Secured({"ROLE_ADMINISTRATOR"})
    public String delete(
            @PathVariable("id") @Nullable final String id
    ) throws AbstractException {
        service.deleteById(id);
        return "redirect:/users";
    }

}
