package ru.t1.aayakovlev.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "loginRequest")
public class AuthLoginRequest {

    @NotNull
    private String username;

    @NotNull
    private String password;

    public AuthLoginRequest(@NotNull final String username, @NotNull final String password) {
        this.username = username;
        this.password = password;
    }

}
