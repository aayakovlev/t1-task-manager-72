package ru.t1.aayakovlev.tm.dto.soap;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public AuthLoginRequest createAuthLoginRequest() {
        return new AuthLoginRequest();
    }

    public AuthLoginResponse createAuthLoginResponse() {
        return new AuthLoginResponse();
    }

    public AuthLogoutRequest createAuthLogoutRequest() {
        return new AuthLogoutRequest();
    }

    public AuthLogoutResponse createAuthLogoutResponse() {
        return new AuthLogoutResponse();
    }

    public AuthProfileRequest createAuthProfileRequest() {
        return new AuthProfileRequest();
    }

    public AuthProfileResponse createAuthProfileResponse() {
        return new AuthProfileResponse();
    }

    public ProjectCountRequest createProjectCountRequest() {
        return new ProjectCountRequest();
    }

    public ProjectCountResponse createProjectCountResponse() {
        return new ProjectCountResponse();
    }

    public ProjectDeleteAllRequest createProjectDeleteAllRequest() {
        return new ProjectDeleteAllRequest();
    }

    public ProjectDeleteAllResponse createProjectDeleteAllResponse() {
        return new ProjectDeleteAllResponse();
    }

    public ProjectDeleteByIdRequest createProjectDeleteByIdRequest() {
        return new ProjectDeleteByIdRequest();
    }

    public ProjectDeleteByIdResponse createProjectDeleteByIdResponse() {
        return new ProjectDeleteByIdResponse();
    }

    public ProjectExistsByIdRequest createProjectExistsByIdRequest() {
        return new ProjectExistsByIdRequest();
    }

    public ProjectExistsByIdResponse createProjectExistsByIdResponse() {
        return new ProjectExistsByIdResponse();
    }

    public ProjectFindAllRequest createProjectFindAllRequest() {
        return new ProjectFindAllRequest();
    }

    public ProjectFindAllResponse createProjectFindAllResponse() {
        return new ProjectFindAllResponse();
    }

    public ProjectFindByIdRequest createProjectFindByIdRequest() {
        return new ProjectFindByIdRequest();
    }

    public ProjectFindByIdResponse createProjectFindByIdResponse() {
        return new ProjectFindByIdResponse();
    }

    public ProjectSaveRequest createProjectSaveRequest() {
        return new ProjectSaveRequest();
    }

    public ProjectSaveResponse createProjectSaveResponse() {
        return new ProjectSaveResponse();
    }

    public ProjectUpdateRequest createProjectUpdateRequest() {
        return new ProjectUpdateRequest();
    }

    public ProjectUpdateResponse createProjectUpdateResponse() {
        return new ProjectUpdateResponse();
    }

    public TaskCountRequest createTaskCountRequest() {
        return new TaskCountRequest();
    }

    public TaskCountResponse createTaskCountResponse() {
        return new TaskCountResponse();
    }

    public TaskDeleteAllRequest createTaskDeleteAllRequest() {
        return new TaskDeleteAllRequest();
    }

    public TaskDeleteAllResponse createTaskDeleteAllResponse() {
        return new TaskDeleteAllResponse();
    }

    public TaskDeleteByIdRequest createTaskDeleteByIdRequest() {
        return new TaskDeleteByIdRequest();
    }

    public TaskDeleteByIdResponse createTaskDeleteByIdResponse() {
        return new TaskDeleteByIdResponse();
    }

    public TaskExistsByIdRequest createTaskExistsByIdRequest() {
        return new TaskExistsByIdRequest();
    }

    public TaskExistsByIdResponse createTaskExistsByIdResponse() {
        return new TaskExistsByIdResponse();
    }

    public TaskFindAllRequest createTaskFindAllRequest() {
        return new TaskFindAllRequest();
    }

    public TaskFindAllResponse createTaskFindAllResponse() {
        return new TaskFindAllResponse();
    }

    public TaskFindAllByProjectIdRequest createTaskFindAllByProjectIdRequest() {
        return new TaskFindAllByProjectIdRequest();
    }

    public TaskFindAllByProjectIdResponse createTaskFindAllByProjectIdResponse() {
        return new TaskFindAllByProjectIdResponse();
    }

    public TaskFindByIdRequest createTaskFindByIdRequest() {
        return new TaskFindByIdRequest();
    }

    public TaskFindByIdResponse createTaskFindByIdResponse() {
        return new TaskFindByIdResponse();
    }

    public TaskSaveRequest createTaskSaveRequest() {
        return new TaskSaveRequest();
    }

    public TaskSaveResponse createTaskSaveResponse() {
        return new TaskSaveResponse();
    }

    public TaskUpdateRequest createTaskUpdateRequest() {
        return new TaskUpdateRequest();
    }

    public TaskUpdateResponse createTaskUpdateResponse() {
        return new TaskUpdateResponse();
    }

    public UserCountRequest createUserCountRequest() {
        return new UserCountRequest();
    }

    public UserCountResponse createUserCountResponse() {
        return new UserCountResponse();
    }

    public UserDeleteAllRequest createUserDeleteAllRequest() {
        return new UserDeleteAllRequest();
    }

    public UserDeleteAllResponse createUserDeleteAllResponse() {
        return new UserDeleteAllResponse();
    }

    public UserDeleteByIdRequest createUserDeleteByIdRequest() {
        return new UserDeleteByIdRequest();
    }

    public UserDeleteByIdResponse createUserDeleteByIdResponse() {
        return new UserDeleteByIdResponse();
    }

    public UserExistsByIdRequest createUserExistsByIdRequest() {
        return new UserExistsByIdRequest();
    }

    public UserExistsByIdResponse createUserExistsByIdResponse() {
        return new UserExistsByIdResponse();
    }

    public UserFindAllRequest createUserFindAllRequest() {
        return new UserFindAllRequest();
    }

    public UserFindAllResponse createUserFindAllResponse() {
        return new UserFindAllResponse();
    }

    public UserFindByLoginRequest createUserFindByLoginRequest() {
        return new UserFindByLoginRequest();
    }

    public UserFindByLoginResponse createUserFindByLoginResponse() {
        return new UserFindByLoginResponse();
    }

    public UserFindByIdRequest createUserFindByIdRequest() {
        return new UserFindByIdRequest();
    }

    public UserFindByIdResponse createUserFindByIdResponse() {
        return new UserFindByIdResponse();
    }

    public UserSaveRequest createUserSaveRequest() {
        return new UserSaveRequest();
    }

    public UserSaveResponse createUserSaveResponse() {
        return new UserSaveResponse();
    }

    public UserUpdateRequest createUserUpdateRequest() {
        return new UserUpdateRequest();
    }

    public UserUpdateResponse createUserUpdateResponse() {
        return new UserUpdateResponse();
    }

}
