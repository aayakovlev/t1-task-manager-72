@XmlSchema(namespace = "http://aayakovlev.t1.ru/tm/dto/soap", elementFormDefault = XmlNsForm.QUALIFIED)
package ru.t1.aayakovlev.tm.dto.soap;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
