package ru.t1.aayakovlev.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    public static String toName(@Nullable final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static Status toStatus(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        return Arrays.stream(values())
                .filter((s) -> value.equals(s.name()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
