package ru.t1.aayakovlev.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.api.service.dto.ITaskDTOService;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.repository.dto.TaskDTORepository;

import java.util.List;
import java.util.Optional;

@Service
public class TaskDTOService implements ITaskDTOService {

    @Getter
    @NotNull
    @Autowired
    private TaskDTORepository repository;

    @Override
    public long countByUserId(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().countByUserId(userId);
    }

    @Override
    @Transactional
    public void deleteAllByUserId(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void deleteByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (!existsByUserIdAndId(userId, id)) throw new TaskNotFoundException();
        getRepository().deleteByUserIdAndId(userId, id);
    }

    @Override
    public boolean existsByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().existsByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUserId(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUserIdAndProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return getRepository().findAllByUserIdAndProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public TaskDTO findByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Optional<TaskDTO> resultEntity = getRepository().findByUserIdAndId(userId, id);
        if (!resultEntity.isPresent()) throw new TaskNotFoundException();
        return resultEntity.get();
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO save(@Nullable final TaskDTO task) throws EntityEmptyException {
        if (task == null) throw new EntityEmptyException();
        return getRepository().save(task);
    }

}
