<jsp:include page="../include/header.jsp"/>
<h3>Login with username and password</h3>
<form action="/auth" method="POST" >
    <div>
        <label for="username" class="no-btm-margin">Username</label>
        <input id="username" name="username" type="text"/>
    </div>
    <div>
        <label for="password" class="no-btm-margin">Password</label>
        <input id="password" name="password" type="password"/>
    </div>
    <button type="submit">Login</button>
</form>

<jsp:include page="../include/footer.jsp"/>

