<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/header.jsp"/>
<h3>PROJECT EDIT</h3>
<form:form action="/projects/edit/${project.id}" method="POST" modelAttribute="project">
    <form:hidden path="id"/>
    <div>
        <p class="no-btm-margin">Project name</p>
        <form:input path="name" type="text"/>
    </div>
    <div>
        <p class="no-btm-margin">Description</p>
        <form:input path="description" type="text"/>
    </div>
    <div>
        <p class="no-btm-margin">Status</p>
        <form:select path="status">
            <form:option value="${null}" label=""/>
            <form:options items="${statuses}" itemLabel="displayName"/>
        </form:select>
    </div>
    <div>
        <p class="no-btm-margin">Created</p>
        <form:input path="created" type="date"/>
    </div>
    <form:button type="submit">Save Project</form:button>
</form:form>
<jsp:include page="../include/footer.jsp"/>
