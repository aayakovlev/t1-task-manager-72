package ru.t1.aayakovlev.tm.integration.endpoint;

import ru.t1.aayakovlev.tm.constant.dto.TaskDTOTestConstant;
import ru.t1.aayakovlev.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.client.TaskRestEndpointClient;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;

import java.util.List;

@Category(IntegrationCategory.class)
public final class TaskRestEndpointTest {

    @NotNull
    final TaskRestEndpointClient client = TaskRestEndpointClient.client();

    @Before
    public void init() {
        client.save(TaskDTOTestConstant.TASK_ONE);
        client.save(TaskDTOTestConstant.TASK_TWO);
        client.save(TaskDTOTestConstant.TASK_THREE);
    }

    @After
    public void finish() {
        client.deleteAll();
    }

    @Test
    public void count() {
        client.save(TaskDTOTestConstant.TASK_FOUR);
        long count = client.count();
        Assert.assertEquals(4, count);
    }

    @Test
    public void deleteAll() {
        client.deleteAll();
        long count = client.count();
        Assert.assertEquals(0, count);
    }

    @Test
    public void deleteById() {
        client.deleteById(TaskDTOTestConstant.TASK_ONE.getId());
        Assert.assertThrows(TaskNotFoundException.class,
                () -> client.findById(TaskDTOTestConstant.TASK_ONE.getId())
        );
    }

    @Test
    public void existsById() {
        final boolean result = client.existsById(TaskDTOTestConstant.TASK_TWO.getId());
        Assert.assertTrue(result);
    }

    @Test
    public void findAll() {
        @NotNull final List<TaskDTO> results = client.findAll();
        Assert.assertNotEquals(0, results.size());
    }

    @Test
    public void findById() {
        @NotNull final TaskDTO result = client.findById(TaskDTOTestConstant.TASK_TWO.getId());
        Assert.assertEquals(TaskDTOTestConstant.TASK_TWO.getName(), result.getName());
    }

    @Test
    public void save() {
        client.save(TaskDTOTestConstant.TASK_FOUR);
        @NotNull final TaskDTO newTask = client.findById(TaskDTOTestConstant.TASK_FOUR.getId());
        Assert.assertEquals(TaskDTOTestConstant.TASK_FOUR.getName(), newTask.getName());
    }

    @Test
    public void update() {
        @NotNull final TaskDTO existedTask = client.findById(TaskDTOTestConstant.TASK_THREE.getId());
        existedTask.setName("new project name");
        client.update(existedTask);
        @NotNull final TaskDTO updatedTask = client.findById(TaskDTOTestConstant.TASK_THREE.getId());
        Assert.assertEquals("new project name", updatedTask.getName());
    }

}
