package ru.t1.aayakovlev.tm.unit.controller;

import lombok.SneakyThrows;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.aayakovlev.tm.api.service.model.IUserService;
import ru.t1.aayakovlev.tm.config.ApplicationConfig;
import ru.t1.aayakovlev.tm.config.DatabaseConfig;
import ru.t1.aayakovlev.tm.config.SecurityConfig;
import ru.t1.aayakovlev.tm.config.WebMVCApplicationConfig;
import ru.t1.aayakovlev.tm.exception.entity.UserNotFoundException;

import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_ADMIN_LOGIN;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_ADMIN_PASSWORD;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.t1.aayakovlev.tm.constant.model.UserTestConstant.*;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class, DatabaseConfig.class, SecurityConfig.class, WebMVCApplicationConfig.class})
public class UserControllerTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IUserService service;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @Before
    @SneakyThrows
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_ADMIN_LOGIN, USER_ADMIN_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        service.save(USER_ONE);
        service.save(USER_TWO);
    }

    @After
    @SneakyThrows
    public void finish() {
        service.deleteById(USER_ONE.getId());
        service.deleteById(USER_TWO.getId());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final String url = "/users";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void delete() {
        service.save(USER_THREE);
        @NotNull final String url = "/users/delete/" + USER_THREE.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertThrows(UserNotFoundException.class, () ->
                service.findById(USER_THREE.getId())
        );
    }

}
