package ru.t1.aayakovlev.tm.unit.repository.dto;

import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.config.ApplicationConfig;
import ru.t1.aayakovlev.tm.config.DatabaseConfig;
import ru.t1.aayakovlev.tm.config.SecurityConfig;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.repository.dto.UserDTORepository;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.*;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class, DatabaseConfig.class, SecurityConfig.class})
public class UserDTORepositoryTest {

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @NotNull
    @Autowired
    private UserDTORepository repository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ONE.setLogin("test_one");
        USER_ONE.setPasswordHash(passwordEncoder.encode("test_one"));
        USER_TWO.setLogin("test_two");
        USER_TWO.setPasswordHash(passwordEncoder.encode("test_two"));
        repository.save(USER_ONE);
        repository.save(USER_TWO);
    }

    @After
    public void finish() {
        repository.deleteById(USER_ONE.getId());
        repository.deleteById(USER_TWO.getId());
    }

    @Test
    public void count() {
        Assert.assertEquals(4, repository.count());
    }


    @Test
    @Transactional
    public void deleteById() {
        repository.save(USER_THREE);
        repository.deleteById(USER_THREE.getId());
        Assert.assertNull(
                repository.findById(USER_THREE.getId()).orElse(null)
        );
    }

    @Test
    public void existsById() {
        Assert.assertTrue(repository.existsById(USER_ONE.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final List<UserDTO> users = repository.findAll();
        Assert.assertEquals(4, users.size());
    }

    @Test
    public void findById() {
        @NotNull final UserDTO user = repository
                .findById(USER_ONE.getId())
                .get();
        Assert.assertEquals(USER_ONE.getId(), user.getId());
        Assert.assertEquals(USER_ONE.getLogin(), user.getLogin());
        Assert.assertEquals(USER_ONE.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void findByLogin() {
        @Nullable final UserDTO user = repository.findByLogin(USER_TWO.getLogin());
        Assert.assertEquals(USER_TWO.getId(), user.getId());
        Assert.assertEquals(USER_TWO.getLogin(), user.getLogin());
        Assert.assertEquals(USER_TWO.getPasswordHash(), user.getPasswordHash());
    }

}
