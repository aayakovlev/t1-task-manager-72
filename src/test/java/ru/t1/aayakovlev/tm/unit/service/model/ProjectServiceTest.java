package ru.t1.aayakovlev.tm.unit.service.model;

import lombok.SneakyThrows;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.aayakovlev.tm.api.service.model.IProjectService;
import ru.t1.aayakovlev.tm.config.ApplicationConfig;
import ru.t1.aayakovlev.tm.config.DatabaseConfig;
import ru.t1.aayakovlev.tm.config.SecurityConfig;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.model.UserRepository;
import ru.t1.aayakovlev.tm.util.UserUtil;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_LOGIN;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_PASSWORD;
import static ru.t1.aayakovlev.tm.constant.model.ProjectTestConstant.*;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class, DatabaseConfig.class, SecurityConfig.class})
public class ProjectServiceTest {

    @NotNull
    @Autowired
    private IProjectService service;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    private User user;

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        user = userRepository.findById(UserUtil.getUserId()).get();
        PROJECT_ONE.setUser(user);
        PROJECT_TWO.setUser(user);
        PROJECT_THREE.setUser(user);
        service.save(PROJECT_ONE);
        service.save(PROJECT_TWO);
    }

    @After
    @SneakyThrows
    public void finish() {
        service.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void count() {
        Assert.assertEquals(2, service.countByUserId(UserUtil.getUserId()));
    }

    @Test
    @SneakyThrows
    public void deleteAll() {
        service.deleteAllByUserId(user.getId());
        Assert.assertEquals(0, service.countByUserId(user.getId()));
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        service.deleteByUserIdAndId(user.getId(), PROJECT_ONE.getId());
        Assert.assertThrows(ProjectNotFoundException.class, () ->
                service.findByUserIdAndId(user.getId(), PROJECT_ONE.getId())
        );
    }

    @Test
    @SneakyThrows
    public void existsById() {
        Assert.assertTrue(service.existsByUserIdAndId(user.getId(), PROJECT_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final List<Project> projects = service.findAllByUserId(user.getId());
        Assert.assertEquals(2, projects.size());
    }

    @Test
    @SneakyThrows
    public void findById() {
        @NotNull final Project project = service.findByUserIdAndId(user.getId(), PROJECT_ONE.getId());
        Assert.assertEquals(PROJECT_ONE.getId(), project.getId());
        Assert.assertEquals(PROJECT_ONE.getName(), project.getName());
        Assert.assertEquals(PROJECT_ONE.getDescription(), project.getDescription());
        Assert.assertEquals(PROJECT_ONE.getStatus(), project.getStatus());
        Assert.assertEquals(PROJECT_ONE.getCreated(), project.getCreated());
    }

}
