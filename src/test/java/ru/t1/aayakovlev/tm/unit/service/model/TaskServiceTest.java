package ru.t1.aayakovlev.tm.unit.service.model;

import lombok.SneakyThrows;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.aayakovlev.tm.api.service.model.ITaskService;
import ru.t1.aayakovlev.tm.config.ApplicationConfig;
import ru.t1.aayakovlev.tm.config.DatabaseConfig;
import ru.t1.aayakovlev.tm.config.SecurityConfig;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.model.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.model.UserRepository;
import ru.t1.aayakovlev.tm.util.UserUtil;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.model.ProjectTestConstant.PROJECT_ONE;
import static ru.t1.aayakovlev.tm.constant.model.TaskTestConstant.TASK_ONE;
import static ru.t1.aayakovlev.tm.constant.model.TaskTestConstant.TASK_TWO;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_LOGIN;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class, DatabaseConfig.class, SecurityConfig.class})
public class TaskServiceTest {

    @NotNull
    @Autowired
    private ITaskService service;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    private User user;

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        user = userRepository.findById(UserUtil.getUserId()).get();
        PROJECT_ONE.setUser(user);
        projectRepository.save(PROJECT_ONE);
        TASK_ONE.setUser(user);
        TASK_ONE.setProject(PROJECT_ONE);
        TASK_TWO.setUser(user);
        TASK_TWO.setProject(PROJECT_ONE);
        service.save(TASK_ONE);
        service.save(TASK_TWO);
    }

    @After
    @SneakyThrows
    public void finish() {
        service.deleteAllByUserId(user.getId());
    }

    @Test
    @SneakyThrows
    public void count() {
        Assert.assertEquals(2, service.countByUserId(user.getId()));
    }

    @Test
    @SneakyThrows
    public void deleteAll() {
        service.deleteAllByUserId(user.getId());
        Assert.assertEquals(0, service.countByUserId(user.getId()));
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        service.deleteByUserIdAndId(user.getId(), TASK_ONE.getId());
        Assert.assertThrows(TaskNotFoundException.class, () ->
                service.findByUserIdAndId(user.getId(), TASK_ONE.getId())
        );
    }

    @Test
    @SneakyThrows
    public void existsById() {
        Assert.assertTrue(service.existsByUserIdAndId(user.getId(), TASK_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final List<Task> tasks = service.findAllByUserId(user.getId());
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findAllByProjectId() {
        @NotNull final List<Task> tasks = service.findAllByUserIdAndProjectId(user.getId(), PROJECT_ONE.getId());
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findById() {
        @NotNull final Task task = service.findByUserIdAndId(user.getId(), TASK_ONE.getId());
        Assert.assertEquals(TASK_ONE.getId(), task.getId());
        Assert.assertEquals(TASK_ONE.getName(), task.getName());
        Assert.assertEquals(TASK_ONE.getDescription(), task.getDescription());
        Assert.assertEquals(TASK_ONE.getStatus(), task.getStatus());
        Assert.assertEquals(TASK_ONE.getCreated(), task.getCreated());
    }

}
